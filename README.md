# Front End Make

An asset pipeline for front end development using GNU Make. Includes css and js compilation/minification, css and js linting, css autoprefixing, svg optimization, and image optimization.

## Requirements

- Node v.12.18.3 or higher
- GNU Make

## Setup/Installation

Clone this repository

```git
git clone https://gitlab.com/grofftech/front-end-make.git
```

Install the npm packages

```bash
npm install
```

## Usage

All commands should be run from a command prompt at the root folder.

Run the default pipeline

```bash
make all
```

Run the default pipeline and watch for changes

```bash
make watch
```

## Tasks

The default folder for all source files is `assets/src`. This can be updated by changing the `SOURCE_DIR` variable in the Makefile.

The default folder for all distribution files is `assets/dist`. This can be updated by changing the `DIST_DIR` variable in the Makefile.

### Sass Compilation / Linting

The source sass files are located in the `src/sass` folder. This can be changed by updating the `CSS_SOURCE_DIR` variable in the Makefile.

The compiled sass files are being outputted to a single CSS file at `dist/css/style.css`. This can be changed by updating the `BUILD_CSS_FILE` variable in the Makefile. This file is then minified using [`cssnano`](https://cssnano.co/) and outputted to `dist/css/style.min.css`. This can be changed by updating the `BUILD_CSS_FILE_MIN` variable in the Makefile.

The non-minified and minified versions of the compiled css file are being copied to the root directory for WordPress compatibility. If this is not needed, remove the code under the `$(BUILD_CSS_FILE)` task in the Makefile.

The files inside the source folder can be organized anyway you like, as long as there is one main file that imports everything needed. This file is configured as `src/sass/styles.scss`. This can be changed by updating the `SASS_FILE` variable in the Makefile.

The sass files are being linted using [`stylelint`](https://stylelint.io/) and the rules that control the linting are in `config/stylelintrc.json`. To prevent any sass files from being linted, add them to the `config/.stylelintignore` file. See [https://stylelint.io/user-guide/configure](https://stylelint.io/user-guide/configure) for configuration options. See [https://stylelint.io/user-guide/rules/list](https://stylelint.io/user-guide/rules/list) for a complete list of rules.

### JavaScript Compilation / Linting

JavaScript files can compiled both as single files and as bundles using [`rollup.js`](https://rollupjs.org/guide/en/).

The JavaScript files are located in the `src/js` folder. This can be changed by updating the `JS_SOURCE_DIR` variable in the Makefile.

To compile single files, update the `JS_FILES` variable in the Makefile with a list of file paths space separated. For better readability if you want to include multiple file paths each with their own new line, add a slash at the end. You can also utilized the `JS_SOURCE_DIR` variable for the file paths.

```bash
JS_FILES="$(JS_SOURCE_DIR)/path/1 \
$(JS_SOURCE_DIR)/path/2 \
$(JS_SOURCE_DIR)/path/3"
```

To compile bundles, you can configure rollup in the `config/rollup.config.js` file. See [https://rollupjs.org/guide/en/#configuration-files](https://rollupjs.org/guide/en/#configuration-files) for more information.

The JavaScript files are being linted using [`eslint`](https://eslint.org/) and the rules that control the linting are in `config/.eslintrc.json`. See [https://eslint.org/docs/user-guide/configuring/](https://eslint.org/docs/user-guide/configuring/) for configuration options. See [https://eslint.org/docs/rules/](https://eslint.org/docs/rules/) for a complete list of rules.

### SVG Optimization

The source SVG files are located in `src/svg`. This can be changed by updating the `SVG_SOURCE_DIR` variable in the Makefile.

The distribution SVG files are located in `dist/svg`. This can be changed by updating the `BUILD_SVG_DIRECTORY`.

The files are being optimized with [`svgo`](https://github.com/svg/svgo) which is then outputting the files to an optimized folder. The optimized folder is `dist/svg/optimized`. This can be updated by changing the `BUILD_SVG_OPTIMIZED_DIR` variable in the Makefile.

Once they are optimized, the files are being converted into a sprite using [`svgstore`](https://github.com/svgstore/svgstore). The sprite file is outputted to `dist/svg/sprite.svg`. This filename can be updated by changing the `BUILD_SVG_SPRITE_FILE` variable in the Makefile.

`Svgo` can be configured using the `config/svgo.config.js` file.

### Image Optimization

Images are being optimized by [`imagemin-cli`](https://github.com/imagemin/imagemin-cli). The only types of images that are being optimized are JPEG and PNG.

The source files for both JPEG and PNG images are located at `src/images`. These can be changed by updating the `IMAGE_FILES_JPEG` and `IMAGE_FILES_PNG` variables in the Makefile.

All optimized image files are being outputted to the `dist/images` folder. This can be changed by updating the `BUILD_IMAGES_DIR` variable in the Makefile.

JPEG images are being optimized using the [`mozjpeg`](https://github.com/imagemin/imagemin-mozjpeg) plugin at 80% quality. If you want to update any options for `mozjpeg`, update the `images-jpg` task in the Makefile. See [https://github.com/imagemin/imagemin-mozjpeg](https://github.com/imagemin/imagemin-mozjpeg) for configuration options. If you would like to use a different plugin for JPEG images, install the plugin with npm, and update the `images-jpg` task in the Makefile.

PNG images are being optimized with no plugins. If you want to add any plugins, install the plugin with npm, and update the `images-png` task in the Makefile.

### Browsersync

Browsersync is run as part of the watch task in the Makefile. The configuration options are located at `config/browser-sync-config.js`. See [https://browsersync.io/docs/options](https://browsersync.io/docs/options) for configuration options.

## Customization

Customize this asset pipeline for specific project needs!
