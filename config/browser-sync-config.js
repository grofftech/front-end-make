/**
 * @file Manages the configuration settings for browser-sync
 * For more information about the options go to http://www.browsersync.io/docs/options/
 */

/**
 * Indicates if the browser-sync snippet needs to be inserted in the DOM
 * someplace other than the default.
 * @type {bool}
 */
var customSnippetPlacement = true;

/**
 * Regular expression for browser-sync to match when inserting the snippet.
 * Only applied if customSnippetPlacement is true.
 * @type {RegEx}
 */
var snippetRegEx = /<script class="-kint-js">/i;

module.exports = {
    "ui": {
        "port": 3001
    },
    "files": [
        "**/*.css",
        "**/*.js",
        "**/*.html",
        "**/*.php"
    ],
    "watchEvents": [
        "change"
    ],
    "watch": false,
    "ignore": [
        "config/browser-sync-config.js",
    ],
    "single": false,
    "watchOptions": {
        "ignoreInitial": true
    },
    "server": false,
    "proxy": "",
    "port": 3000,
    "https": true,
    "middleware": false,
    "serveStatic": [],
    "ghostMode": {
        "clicks": true,
        "scroll": true,
        "location": true,
        "forms": {
            "submit": true,
            "inputs": true,
            "toggles": true
        }
    },
    "logLevel": "info",
    "logPrefix": "Browsersync",
    "logConnections": false,
    "logFileChanges": true,
    "logSnippet": true,
    "snippetOptions": {
        rule: {
            match: snippetRegEx,
            fn: function (snippet, match) {
                if (customSnippetPlacement)
                {
                    return snippet + match;
                }
                else {
                    return;
                }
            }
        }
    },
    "rewriteRules": [],
    "open": false,
    "browser": "default",
    "cors": false,
    "xip": false,
    "hostnameSuffix": false,
    "reloadOnRestart": false,
    "notify": true,
    "scrollProportionally": true,
    "scrollThrottle": 0,
    "scrollRestoreTechnique": "window.name",
    "scrollElements": [],
    "scrollElementMapping": [],
    "reloadDelay": 0,
    "reloadDebounce": 500,
    "reloadThrottle": 0,
    "plugins": [],
    "injectChanges": true,
    "startPath": null,
    "minify": true,
    "host": "wordpress.local",
    "localOnly": false,
    "codeSync": true,
    "timestamps": true,
    "clientEvents": [
        "scroll",
        "scroll:element",
        "input:text",
        "input:toggles",
        "form:submit",
        "form:reset",
        "click"
    ],
    "socket": {
        "socketIoOptions": {
            "log": false
        },
        "socketIoClientConfig": {
            "reconnectionAttempts": 50
        },
        "path": "/browser-sync/socket.io",
        "clientPath": "/browser-sync",
        "namespace": "/browser-sync",
        "clients": {
            "heartbeatTimeout": 5000
        }
    },
    "tagNames": {
        "less": "link",
        "scss": "link",
        "css": "link",
        "jpg": "img",
        "jpeg": "img",
        "png": "img",
        "svg": "img",
        "gif": "img",
        "js": "script"
    }
};