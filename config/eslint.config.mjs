// https://eslint.org/docs/latest/use/configure/configuration-files
import js from "@eslint/js";

export default [
    js.configs.recommended,
    {
        "files": [""], // Does not work, add --files in Makefile
        "ignores": [], // Does not work, add --ignore-pattern in Makefile
        "languageOptions": {
            ecmaVersion: "latest",
            scriptType: "module",
            globals: {
                // How do you configure this?
            }

        },
        "rules": {
            "indent": [
                "error",
                4,
                {
                    "ObjectExpression": "first",
                    "ArrayExpression": "first"
                }
            ],
            "linebreak-style": [ "error", "unix" ],
            "no-console": [
                "error",
                {
                    "allow": ["warn", "error"]
                }
            ],
            "quotes": [
                "error",
                "double",
                {
                    "allowTemplateLiterals": true,
                    "avoidEscape": true
                }
            ],
            "semi": ["error", "always"]
        },
    }
];