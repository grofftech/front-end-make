import { terser } from "rollup-plugin-terser";

export default {
    input: "assets/src/js/modules/main.js",
    output: [
        {
            file: "assets/dist/js/scripts.js",
            format: "iife"
        },
        {
            file: "assets/dist/js/scripts.min.js",
            format: "iife",
            plugins: [terser()]
        }
    ]
};