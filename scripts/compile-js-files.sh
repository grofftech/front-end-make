#!/bin/bash
# Compile JS Files
# Copyright 2021 groffTECH
#
# This script takes in a list of individual JavaScript files which then copies
# each file unaltered to an output directory and then minifies each
# file (using the Terser npm package) and outputs the minified version to
# the same output directory.
#
# It is run in context from the Makefile in the root directory
# and the arguments are supplied from there.
TERSER=node_modules/.bin/terser
JS_FILES=$1
BUILD_JS_DIR=$2

if [ -n "$JS_FILES" ]; then
    for f in $JS_FILES
    do
        if [ -f "$f" ]; then
            FILENAME_WITH_EXTENSION="${f##*/}"
            FILENAME_WITHOUT_EXTENSION="${FILENAME_WITH_EXTENSION%.*}"

            cp -v "$f" "$BUILD_JS_DIR"/"$FILENAME_WITH_EXTENSION"

            $TERSER "$f" \
                --compress \
                --mangle \
                --source-map \
                --output "$BUILD_JS_DIR"/"$FILENAME_WITHOUT_EXTENSION".min.js
        fi
    done
fi